from __future__ import absolute_import, unicode_literals

import os

import celery

os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'logic_server.settings')

app = celery.Celery('logic_server')
app.config_from_object('django.conf:settings', namespace='CELERY')

app.autodiscover_tasks()
