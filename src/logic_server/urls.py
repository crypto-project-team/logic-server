from django.contrib import admin
from django.urls import include, path

from logic import urls as logic_urls

urlpatterns = [
    path('api/', include(logic_urls)),
    path('admin/', admin.site.urls),
]
