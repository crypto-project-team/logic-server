from django import apps


class LogicAppConfig(apps.AppConfig):
    name = 'logic_server'
