from django.contrib import admin

from logic.models import EncryptedTask


@admin.register(EncryptedTask)
class EncryptedTaskAdmin(admin.ModelAdmin):
    pass
