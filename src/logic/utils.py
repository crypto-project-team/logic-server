import os
from typing import Tuple

import requests
from django.conf import settings
from rest_framework.request import Request


def get_upload_path(instance, filename):
    return os.path.join(settings.FILES_UPLOAD_TO, filename)


def get_client_ip(request: Request) -> Tuple[str, str]:
    x_forwarded_for = request.META.get('HTTP_X_FORWARDED_FOR')
    if x_forwarded_for:
        ip = x_forwarded_for.split(',')[0]
        port = '8080'
    else:
        ip = request.META.get('REMOTE_ADDR')
        port = '8080'
    return ip, port


class Sender:
    __slots__ = '__token', '__server'
    __token: str
    __server: str

    def __init__(self, token: str, server: str) -> None:
        self.__token = token
        self.__server = server

    def send_file(self, file_path: str, task_id: int) -> int:
        url = f'http://{self.__server}/api/result/'
        r = requests.post(
            url,
            files={'file': open(file_path, 'rb')},
            data={'task_id': task_id},
            headers={f'Authorization': f'Token {self.__token}'})
        return r.status_code
