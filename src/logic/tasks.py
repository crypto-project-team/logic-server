import os
from time import time
import zipfile

from django.core.files import File
from django.conf import settings

from logic.models import EncryptedTask, EncryptedResult
from logic.utils import Sender
from logic_server.celery import app as celery_app
from crypto_library.cloud import process_expression


@celery_app.task(name='calculation.send_result')
def send_result(result_id: int, token: str, client_ip: str) -> None:
    encrypted_result: EncryptedResult = EncryptedResult.objects.get(pk=result_id)
    print(f'START::Sending result: {encrypted_result}')
    result = EncryptedResult.objects.get(pk=result_id)
    task: EncryptedTask = EncryptedTask.objects.get(result_id=result_id)
    sender = Sender(token, client_ip)
    sender.send_file(result.file.path, task.task_id)
    print(f'END::Sending result: {encrypted_result}')


@celery_app.task(name='calculation.run_task')
def run_task(task: str, token: str, client_ip: str) -> None:
    print(f'START::Run task: {task} from {client_ip}')
    if not task.isdigit():
        raise Exception(f'Bad task: {task}')
    task_id = int(task)
    encrypted_task: EncryptedTask = EncryptedTask.objects.get(pk=task_id)
    t_start = time()
    print(f'CALCULATING::{encrypted_task}')
    result_path_file = calculating(encrypted_task)
    print(f'CONTINUE::{encrypted_task}')
    result = EncryptedResult.objects.create(user=encrypted_task.user)
    with open(result_path_file, 'rb') as file:
        result.file = File(file, name=os.path.basename(file.name))
        result.save()
    encrypted_task.result = result
    encrypted_task.save()
    send_result.delay(result.pk, token, client_ip)
    print(f'END::{encrypted_task}, {(time() - t_start) * 1e3:.1f} ms')


def calculating(task: EncryptedTask) -> str:
    encrypted_nums = task.file
    dir_path = os.path.join(settings.MEDIA_ROOT, f'task_{task.pk}')
    with zipfile.ZipFile(encrypted_nums, 'r') as zip_ref:
        zip_ref.extractall(dir_path)
    process_expression(dir_path, settings.CLOUD_PATH)
    return f'{dir_path}/answer'
