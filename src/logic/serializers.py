from rest_framework import serializers

from logic.models import EncryptedTask


class EncryptedTaskSerializer(serializers.ModelSerializer):
    class Meta:
        model = EncryptedTask
        fields = ('pk', 'user', 'created', 'file')

    def create(self, validated_data, *args, **kwargs):
        user = self.context['request'].user
        validated_data['user'] = user
        task_id = self.context['request'].data['task_id']
        validated_data['task_id'] = task_id
        obj = super().create(validated_data)
        return obj
