from django.utils.decorators import method_decorator
from django.views.decorators.csrf import csrf_exempt
from rest_framework import status
from rest_framework.authtoken.models import Token
from rest_framework.authtoken.views import ObtainAuthToken
from rest_framework.permissions import IsAuthenticated
from rest_framework.request import Request
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework.viewsets import ModelViewSet

from logic.models import EncryptedTask
from logic.serializers import EncryptedTaskSerializer
from logic.tasks import run_task
from logic.utils import get_client_ip


class EncryptedTaskViewSet(ModelViewSet):
    permission_classes = (IsAuthenticated,)
    queryset = EncryptedTask.objects.all()
    serializer_class = EncryptedTaskSerializer
    http_method_names = ['get', 'post']


class RunEncryptedTaskViewSet(APIView):
    def get(self, request: Request, *args, **kwargs):
        if 'task' in request.data and 'token' in request.data:
            client_ip, client_port = get_client_ip(request)
            print(f'Run task::{request.data["task"]}')
            run_task.delay(request.data['task'], request.data['token'], f'{client_ip}:{client_port}')
            return Response(f'got from: {client_ip}', status=status.HTTP_200_OK)
        else:
            return Response('Error: no task', status=status.HTTP_400_BAD_REQUEST)


@method_decorator(csrf_exempt, name='dispatch')
class AuthToken(ObtainAuthToken):
    name = None

    def post(self, request, *args, **kwargs):
        serializer = self.serializer_class(data=request.data)
        serializer.is_valid(raise_exception=True)
        user = serializer.validated_data['user']

        token, created = Token.objects.get_or_create(user=user)

        user_groups = user.groups.all()
        groups = [str(g) for g in user_groups]
        permissions = [group.permissions.all() for group in user_groups]
        permissions = [item.codename for sublist in permissions for item in sublist]

        return Response({
            'username': user.username,
            'id': user.id,
            'token': str(token),
            'groups': groups,
            'permissions': permissions,
        })
