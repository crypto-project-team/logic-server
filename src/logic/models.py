from django.conf import settings
from django.db import models

from logic.utils import get_upload_path


class EncryptedResult(models.Model):
    user = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.SET_NULL, blank=True, null=True)
    created = models.DateTimeField('Дата создания', auto_now_add=True)
    file = models.FileField('Файл', upload_to=get_upload_path)

    def __str__(self) -> str:
        return f'id: {self.pk}, user: {self.user}, created:{self.created}'

    class Meta:
        verbose_name = 'Результат'
        verbose_name_plural = 'Результаты'


class EncryptedTask(models.Model):
    file = models.FileField('Файл', upload_to=get_upload_path)
    task_id = models.IntegerField('Task')
    user = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.SET_NULL, blank=True, null=True)
    created = models.DateTimeField('Дата создания', auto_now_add=True)
    result = models.OneToOneField(EncryptedResult, blank=True, null=True, on_delete=models.CASCADE)

    def __str__(self) -> str:
        return f'id: {self.pk}, user: {self.user}, created:{self.created}'

    class Meta:
        verbose_name = 'Зашифрованная задача'
        verbose_name_plural = 'Зашифрованные задачи'
