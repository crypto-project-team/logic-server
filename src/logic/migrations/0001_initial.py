# Generated by Django 3.0 on 2019-12-12 02:03

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion
import logic.utils


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='EncryptedResult',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created', models.DateTimeField(auto_now_add=True, verbose_name='Дата создания')),
                ('file', models.FileField(upload_to=logic.utils.get_upload_path, verbose_name='Файл')),
                ('user', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'verbose_name': 'Результат',
                'verbose_name_plural': 'Результаты',
            },
        ),
        migrations.CreateModel(
            name='EncryptedTask',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('file', models.FileField(upload_to=logic.utils.get_upload_path, verbose_name='Файл')),
                ('task_id', models.IntegerField(verbose_name='Task')),
                ('created', models.DateTimeField(auto_now_add=True, verbose_name='Дата создания')),
                ('result', models.OneToOneField(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='logic.EncryptedResult')),
                ('user', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'verbose_name': 'Зашифрованная задача',
                'verbose_name_plural': 'Зашифрованные задачи',
            },
        ),
    ]
