from django.conf import urls
from rest_framework.routers import DefaultRouter

from .views import *

router = DefaultRouter()
router.register('upload_calculation', EncryptedTaskViewSet, 'EncryptedTask')

urlpatterns = [
    urls.url('run_calculation/', RunEncryptedTaskViewSet.as_view(), name='RunEncryptedTask'),
    urls.url(r'^token-auth/', AuthToken.as_view(name='auth')),
]

urlpatterns += router.urls
